# Projects - ARC

## SDR - Demo

### decode POCSEG

- run GQRX and tune to the correct band
	- turn on UDP
- [multimon-ng](https://github.com/EliasOenal/multimon-ng) compile and install.
- Pull data for decording  `nc -l -u 7355 |     sox -t raw -esigned-integer -b16 -r 48000 - -esigned-integer -b16 -r 22050 -t raw - | multimon-ng -t raw -a POCSAG512 -a POCSAG1200 -a POCSAG2400 -f alpha -`
  
### FT8 
