# Configuring Echolink

## Basic installation

Running an echolink node on a Raspi.
- insatll svxlink following a online guide.
- Disable internal audio cards: edit `/boot/config.txt`
	- change line "dtoverlay=vc4-kms-v3d" to "dtoverlay=vc4-kms-v3d,noaudio"
	-	omitting "dtparam=audio=on"

## Hardware - setup

- Components:
	- Baufeng - 1
	- 3.5 mm + 2.5 mm cable for kenwood connector
	- USB sound card
	- MOSFET for PPT
	-	0.1 uf cap
	- 10 k 

- Schematic
<img src="./res/schematic.png" width="40%" />

- Current setup is designed for a disposable HT (A feng)
- Features:
	- [X] PPT with serial DTR
	- [X] DTMF basic commands

## Software configuration 

- Using Simplexlogic in svxlink
- `src/svxlink.conf` know good configuration
- `src/Module*.tlc` customized 

## Debug info

- `tail -f /var/log/svxlink` is the logfile used. gives information onthe service
- `sudo systemcts <> svxlink` control the service 
- In the case on a busy server current fix is change to a different server. A list of public servers are available [here](https://www.echolink.org/proxylist.jsp)


### Todo

- Weather information 
- PTR status (Just for fun)
- Change public proxy based on available. Currently using a constant proxy. (add this as a prescript file in the service)
- mqtt commands
	- kill command
	- change mqtt server if primary is not available

### Known bugs

-	On restart DTR pin is left hanging till the svxlink service is started.(Posible fix is use RST pin to turn off power to the radio)
- DTMF detection on the repeater is not as easy as simplex.
- Disable Pirrot since it starts cyclic process.
- 
