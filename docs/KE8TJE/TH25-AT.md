
- Got a radio for $10 from the Flatwoods hamfest of 27/07/2024
- Cleaning up and testing went well
- There was no tone board installed in the radio. To be useful in modern days I need to have a tone board installed in there
	- TSU-6 is the original part. This is working using a MN6520 and a MN4094BS (serial to parallel)
	- MN6520 DIY: [here][https://www.zl2pd.com/CML_PW_50tone_CTCSS_Encoder.html]

## Original schematic

![](../../ref/Pasted%20image%2020240727211444.png)

## DIY MN6520 schematic

![](../../ref/Pasted%20image%2020240809214655.png)

## CD 4094 

![|300](../../ref/Pasted%20image%2020240809214830.png)

- data sheet: [here][https://www.ti.com/lit/ds/symlink/cd4094b.pdf?HQS=dis-dk-null-digikeymode-dsf-pf-null-wwe&ts=1723254569914&ref_url=https%253A%252F%252Fwww.ti.com%252Fgeneral%252Fdocs%252Fsuppproductinfo.tsp%253FdistId%253D10%2526gotoUrl%253Dhttps%253A%252F%252Fwww.ti.com%252Flit%252Fgpn%252Fcd4094b]
- SMD package: [mouser][https://www.mouser.com/ProductDetail/Texas-Instruments/CD4094BNSR?qs=LSR%252BNeaw%2FwDPtwxG7pPxXQ%3D%3D]



