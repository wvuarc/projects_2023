### Pre-net

- (five minutes before net)
	Is the frequency in use? This is (call sign).
- [Pause]
	This is `<call sign>` The W8CUL Traffic & Information Net will begin in approximately FIVE minutes.
- [One minute before net]
	This is `<call sign>` The W8CUL Traffic & Information Net will begin in approximately ONE minute.
###  Net Operation
#### Preamble 
- CQ, CQ, CQ. This is (call sign), Net Control Operator for the W8CUL net. My name is (name)(using phonetics), and  I am located in (city), (State). This net meets every Thursday evening at 2100 local time on the  146.760 MHz repeater with a 103.5 Hz PL tone for the purpose of planning, practicing and  discussing the activities of WVU Amateur Radio Club.
- [Pause]
	Is an Alternate Net Control Station on frequency? 
	(Acknowledge ANCS, or ask for volunteer if no  answer. Allow the ANCS an opportunity to formally check-in at this time.)
- This is a directed net. Please do not transmit unless called by Net Control. During the net,  please direct all communications through Net Control, (call sign).
- Please check out with Net Control if you should need to leave the net prior to closing.
- This net is open to anyone who is licensed to operate on this repeater and is interested in  amateur radio communications.
- All stations will have an opportunity to check in; however, please wait for the appropriate  check-in group. We will begin with Short Time, then W8CUL Officers finally general check-ins.
- When called for check-in, please say "This is", then un-key and listen. If you do not hear another  station, continue your transmission providing: your call sign, first alphabetically then again with international phonetics
- Please check-in four at a time, allowing Net Control to recognize the stations that have checked  in.
- Any Emergency or Priority traffic will be handled first during this net. I will now break for any  emergency or priority traffic --- emergency or priority traffic only please call now. (ONLY CALL ONCE)
- [Pause]
	(Allow time for multiple stations)
	(If there is no emergency or priority traffic, say "Nothing heard" and continue)
	If you need to pass emergency traffic at any time during this net, please notify Net Control and  your traffic will be handled accordingly.

- Are there any stations with formal written Traffic? (ONLY CALL ONCE)

- [Pause] 
	(Allow time for multiple stations)
	(Record Traffic on ARRL Radiogram Form)
	(If there is no emergency or priority traffic, say "Nothing heard" and continue)
---
#### Check-ins

- We will now begin taking check-ins. We will begin with Short Time, general check-ins and finally any Echo link stations.
- Short Time Stations only please call now. (ONLY CALL ONCE)
- [Pause] (Allow time for multiple stations to check in.)
	(If you have any checks get any comments after  and allow them to leave the net by saying. “You are allowed the secure at any time”
- [Pause]
	We will now take general check-ins. If you have not yet checked in and wish to do so, please call  now.
	(recognize stations four at a time, and call for more checkins if regular stations have not checked in)
- [Pause]
	(Go down the list of stations and ask for comments one by one. If there is a new station ask for name and location)
- [Pause]
	Are there any echo link stations. (Allow extra time to pass between talking to echolink stations to account for the network delay)
	(Recognize and ask for any comments)
- [Pause]

- Would any station like to comment on anything they’ve hear up until now? (ONLY CALL ONCE) (Call on any stations that respond. Repeat as needed.)
- Before closing the net, are there any additional stations who would like to check in? If so,  please call now. (Announce Final Check-In Twice)
- [Pause]
	(Log and recognize any late check-ins)
- This evening we had (check-in count)check-ins, including myself. (alternate callsign) can you confirm this count.
#### End of  net
- Thank you to everyone who participated in this evening's W8CUL Traffic & Information net.  Thanks, to the WVU Amateur Radio Club for maintaining their repeaters for public service  communications. The next scheduled net will be next Thursday (month/day) at 2100 local time.
- This is (callsign), closing the net at (local time) pm local time and returning the  repeater to normal use.
- Record the end of net time in the net log