
# Content - Check list

### Basic
- [ ] Radio + Mic
- [ ] Battery 
- [ ] Wire with battery leads
- [ ] Slim Jim and rope
- [ ] Mini voltmeter 
### For a mobile setup 
- [ ] 12 V adapter
- [ ] Mobile antena and mount 
## Setting up the Slim Jim

- **Need to be away from metal** as much as possible. Trees work great if you are out doors
- These work best when they are stretched to its full length
	- Find a weight and attach it to the bottom end of the rope. (ie. rock,Jug of water, EMCOM kit itself)
	- Make at least 3 loops as indicated including the **very top and bottom rungs** of the ladder line
		![](../ref/Pasted%20image%2020240712101218.png)

---

# GRUSK 2024


## KE8TJE - Notes

- Put string in the EMCOM kits
	- Educate how to use the slim Jim better 
	- Most of us did fine
- Modifications to the base station setup
	- Setup a cross band repeat on a radio with an antenna high up a tree.
	- run with AC power
	- cross band repeater on GMRS frequency (is this legal if we get a GMRS license  )
	- This should allow more people to listen in and talk if needed. Specially timing folks
		- I had to do a lot of walking to find stuff out