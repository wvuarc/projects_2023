
## FT8 - 20 m active times

This is based on spotting activity.

### Alaska contacts (times in EST)

- [KL4JP](https://www.pskreporter.info/pskmap?callsign=KL5JQ&search=Find) : 2000 h
- [KL5JQ](https://www.pskreporter.info/pskmap?callsign=KL5JQ&search=Find):  0000 h 
- [KL7RRC](https://www.pskreporter.info/pskmap?callsign=KL7RRC&search=Find) : 1300 - 1500 h

### Hawaii

- NH6V:
- KH6CW

## Europe DX times

- February SCR: 8 - 12 AM is the best propagation for Germany/Amsterdam/England