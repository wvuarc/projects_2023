
## COM settings for N1MM+

- Baud: 34000
- DTR,RST: always off (default settings put the radio in to TX mode. which is not needed)
- If you se the Waterfall getting laggy/freeze potentially these settings are wrong
- If you are using wxjt with N1MM disable the com settings in M1MM

