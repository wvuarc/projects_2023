*Last updated: 14/12/2023 by KE8TJE,*

This guide is written to be followed when using W8CUL radios to setup and get FT8 operational with your own call sign. If you are using the club call sign most of this configuration would be done for you.

## Our current setup

- Radios: 
	- Elecraft K3s
	- ICOM 9100 
- Amps:
	- ICOM : need model here, [maunal](./#)
	-  SB-200 ?? : [manual](./#) 
## Applications 

Depending on what you are interested in doing you may chose one of the following sections and follow along

### FT8 Basic operation (No logging)

- Similar to SSB voice operations first listen to the FT8 frequency you want to operate on
	- 20 m: 14.074
	- 10 m: 7.074
- FT8 is a data mode that takes 15 s to transmit one segment of information. (13 s is used for actual transmission and the rest is typically used for decoding the messages) You may typically hear different intensity levels/tones during these 15 s slots and that is expected
- Once you can confirm the radio is operating as expected with the antenna its time to set up WXJT-X
- under file > Settings > Radio
	- configuration for Elecaraft: Select radio and defaults should work for this
	
	 ![|400](../ref/Pasted%20image%2020231214170905.png)

	- Configuration for ICOM 9100: Based on the manual/Testing
	  - Baud: 19200
	  - Data bits: 8
	  - Stop bits: 2
	  - Handshake: None
	  - PPT Method: CAT
	  - mode: Data/Pkt
	  - Split operation: Rig
	  
- To use a personal call: you can clone the settings form the **W8CUL** profile. 
	- Configurations > W8CUL > Clone. 
	- once cloned rename the profile and you can switch to that profile for further configuration.
- Next Check the Audio tab to see if the correct Mic and Speaker I/O are selected.

### FT8 with logging

- If you are logging with N1MM+ WXJT can be integrated into sending and receiving information directly
- **DO NOT** open WXJT via the desktop shortcut. It can be launched via N1MM. 
- Make sure to change the operator call sign and the database used in N1MM
- Export adif logs to a separate location so other users can use freely 
	- I personally store them in git since they are text files and make it easy to sync between devices


## Debugging

### WXJT 

- Radio is not keying up
	- Probably the wrong com port
- No activity on the water fall
	- In correct audio port
- links: 
	- 
